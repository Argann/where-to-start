# Par où commencer ?

Ça y est, un nouveau client a été trouvé, l'équipe est prête et motivée, et les premiers documents de réflexion autour du Game Design commencent déjà à tomber. En tant que développeur, vous allez devoir rapidement mettre en place un nouveau projet Unity. 

Vous pourriez très bien simplement créer un projet vide à partir du _Unity Hub_ mais, soyons honnête, vous n'avez pas vraiment envie de tout recommencer de zéro une énième fois.

Heureusement, ce dépôt est là pour vous !

## Démarrage rapide

Vous n'avez pas le temps de lire la magnifique documentation fournie ? Vous avez des délais _très_ serrés ? Pour commencer à travailler sans tarder, vous n'avez qu'à suivre ces indications :

- Installez sur votre poste de travail la version de Unity `2019.4.16f1 LTS` (via le Unity Hub par exemple)
- Créez un _fork_ de ce dépôt, et adaptez son nom, son dossier et sa description à vos besoins
- Clonez le projet en local, et modifiez ce fichier `README.md` afin qu'il présente votre nouveau projet
- Tentez d'ouvrir le projet avec votre version de Unity, et assurez-vous qu'il ne présente aucune erreur
- Créez un commit et poussez, vous voilà prêts !

## Documentation

Ce dépôt n'est pas qu'un projet Unity bête et méchant ! 

[C'est aussi un wiki bête et méchant.](../../wikis/home) 

Il documente :
- L'architecture de projet Unity proposée dans ce dépôt
- Les _pipelines_ d'intégration et de déploiement continus
- Un _processus_ de travail permettant d'appliquer les _best practices_ d'ingénierie informatique ^(enfin, on essaye)^
