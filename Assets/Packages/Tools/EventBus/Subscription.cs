using System;

namespace GameEventBus
{
    /// <summary>
    ///     Implementation of <seealso cref="ISubscription"> for a specific
    ///     type of game events.
    /// </summary>
    /// <typeparam name="TGameEvent">
    ///     Type of the game event that the listener associated with this 
    ///     subscription need as a parameter.
    /// </typeparam>
    internal class Subscription<TGameEvent> : ISubscription where TGameEvent : GameEvent
    {
        #region Fields
        /// <summary>
        ///     Method that must be called when this subscription is 
        ///     invoked.
        /// </summary>
        Action<TGameEvent> _action;
        #endregion

        #region Properties
        /// <summary>
        ///     Property that retrieves the method called when this 
        ///     subscription is invoked.
        ///     <br/>
        ///     Read-only.
        /// </summary>
        /// <value></value>
        public object Action
        {
            get => _action;
        }
        #endregion

        #region Constructor
        /// <summary>
        ///     Base constructor for a new instance of Subscription.
        /// </summary>
        /// <param name="action">
        ///     Method that will be triggered when this subscription is
        ///     invoked.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if the param 'action' is null.
        /// </exception>
        public Subscription(Action<TGameEvent> action)
        {
            if (action == null)
                throw new ArgumentNullException("action", "Can't initialize a new Subscription: the given action is null.");

            _action = action;
        }
        #endregion

        #region Methods
        /// <summary>
        ///     Invoke this instance of subscription, and call its action.
        /// </summary>
        /// <param name="eventInstance">
        ///     Instance of the game event that will be send to the action
        ///     linked to this subscription.
        ///     <br/>
        ///     Must not be null.
        ///     <br/>
        ///     Must be of the same GameEvent sub-type as this subscription.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if the param 'eventInstance' is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     Thrown if the param 'eventInstance' is not of the same type as
        ///     this subscription event type.
        /// </exception>
        public void Invoke(GameEvent eventInstance)
        {
            if (eventInstance == null)
                throw new ArgumentNullException("eventInstance", "Can't invoke this subscription: the event instance is null.");

            TGameEvent castedEvent = eventInstance as TGameEvent;

            if (castedEvent == null)
                throw new ArgumentException("Can't invoke this subscription: the event instance is not of the same type as this subscription.", "eventInstance");

            _action.Invoke(castedEvent);
        }
        #endregion
    }
}