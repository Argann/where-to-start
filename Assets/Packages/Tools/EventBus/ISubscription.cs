namespace GameEventBus
{
    /// <summary>
    ///     Interface that defines the contract that every type of
    ///     subscription to a game event must follow.
    ///     It's only used internally by the Event Bus system.
    /// </summary>
    internal interface ISubscription
    {
        /// <summary>
        /// Method that will be called when this subscription is 
        /// invoked.
        /// </summary>
        object Action {get;}

        /// <summary>
        /// Invoke this subscription by calling the linked method
        /// with the specified instance of event as a parameter.
        /// </summary>
        /// <param name="eventInstance"></param>
        void Invoke(GameEvent eventInstance);
    }
}