namespace GameEventBus
{
    /// <summary>
    ///     Base abstract class for all the game events classes.
    /// </summary>
    public abstract class GameEvent {}
}