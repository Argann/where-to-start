﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameEventBus
{
    /// <summary>
    ///     Message bus that handle everything related to subscription,
    ///     unsubscription and invocation of game events.
    /// </summary>
    public class EventBus
    {
        #region Fields
        /// <summary>
        ///     Dictionary that maps a type of game event to a set of
        ///     actions that listens to this particuliar type of events.
        /// </summary>
        private readonly Dictionary<Type, List<ISubscription>> _subscriptions;
        #endregion

        #region Constructor
        /// <summary>
        ///     Public constructor that initialize a brand new instance of
        ///     an Event Bus.
        /// </summary>
        public EventBus()
        {
            _subscriptions = new Dictionary<Type, List<ISubscription>>();
        }
        #endregion

        #region Methods
        /// <summary>
        ///     Add a listener that will be called as soon as the type of event
        ///     specified will be invoked.
        /// </summary>
        /// <param name="listener">
        ///     Method that takes an instance of the specified game event that will
        ///     be called when this type of event is invoked.
        /// </param>
        /// <typeparam name="TGameEvent">
        ///     Type of the game event that will trigger the call to the specified listener. 
        /// </typeparam>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if the specified listener is null.
        /// </exception>
        public void AddListener<TGameEvent>(Action<TGameEvent> listener) where TGameEvent : GameEvent
        {
            if (listener == null)
                throw new ArgumentNullException("listener", "Can't add listener to a specific type of event because the listener provided is null.");

            if (!_subscriptions.ContainsKey(typeof(TGameEvent)))
                _subscriptions.Add(typeof(TGameEvent), new List<ISubscription>());
            
            if (_subscriptions[typeof(TGameEvent)].Any(x => x.Action.Equals(listener)))
                return;
            
            _subscriptions[typeof(TGameEvent)].Add(new Subscription<TGameEvent>(listener));
        }

        /// <summary>
        ///     Remove a method from the list of listeners for a 
        ///     specified type of event.
        ///     <br/>
        ///     If the method is not present in the list of listeners, 
        ///     it will throw an error.
        /// </summary>
        /// <param name="listener">
        ///     Method that must be removed from the list of listeners.
        ///     <br/>
        ///     Must not be null.
        ///     <br/>
        ///     Must be in the listeners list for this type of game event.
        /// </param>
        /// <typeparam name="TGameEvent">
        ///     Type of the game event to remove the listener from.
        /// </typeparam>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if the param 'listener' is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     Thrown if the type of game event specified has no 
        ///     listeners associated to it.
        /// </exception>
        /// <exception cref="InvalidOperationException">
        ///     Thrown if the param 'listener' is not in the listeners list 
        ///     associated to this type of game event.
        /// </exception>
        public void RemoveListener<TGameEvent>(Action<TGameEvent> listener) where TGameEvent : GameEvent
        {
            if (listener == null)
                throw new ArgumentNullException("listener", "Can't remove listener for a specific type of event because the listener provided is null.");

            if (!_subscriptions.ContainsKey(typeof(TGameEvent)))
                throw new ArgumentException("Can't remove listener for a specific type of event because this type of event has no listener.", "listener");
            
            ISubscription subscription = _subscriptions[typeof(TGameEvent)].FirstOrDefault(x => x.Action.Equals(listener));

            if (subscription == null)
                throw new InvalidOperationException("Can't remove listener for a specific type of event because the listener was not added to this event in the first place.");

            _subscriptions[typeof(TGameEvent)].Remove(subscription);
        }

        /// <summary>
        ///     Remove every listeners that are associated with a specified
        ///     type of game events.
        /// </summary>
        /// <typeparam name="TGameEvent">
        ///     Type of game event that will be cleared of all its listeners.
        /// </typeparam>
        public void RemoveAllListeners<TGameEvent>() where TGameEvent : GameEvent
        {
            if (!_subscriptions.ContainsKey(typeof(TGameEvent)))
                return;
            
            _subscriptions[typeof(TGameEvent)].Clear();
        }

        /// <summary>
        ///     Invoke a game event through this Event Bus, and call all the listeners
        ///     associated with this type of game event.
        /// </summary>
        /// <param name="gameEvent">
        ///     Instance of the event that will be send to all the listeners of this
        ///     type of game event.
        ///     <br/>
        ///     Must not be null.
        /// </param>
        /// <typeparam name="TGameEvent">
        ///     Type of the game event sent through this event bus.
        /// </typeparam>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if the param 'gameEvent' is null.
        /// </exception>
        public void Invoke<TGameEvent>(TGameEvent gameEvent) where TGameEvent : GameEvent
        {
            if (gameEvent == null)
                throw new ArgumentNullException("gameEvent", "Can't invoke event, because the event is null.");

            if (!_subscriptions.ContainsKey(typeof(TGameEvent)))
                return;
            
            foreach (var subscription in _subscriptions[typeof(TGameEvent)])
            {
                try
                {
                    subscription.Invoke(gameEvent);
                }
                catch (System.ArgumentException)
                {
                    Debug.LogError($"Can't properly invoke event '{typeof(TGameEvent)}': there is an error in the integrity of the event bus.");
                }
            }
        }
        #endregion
    }
}


