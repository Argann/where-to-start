using GameEventBus;
using Zenject;

namespace WhereToStart.Components.Installers
{
    /// <summary>
    ///         Components used by Extenject to define how it
    ///         must inject the events to the rest of the project.
    /// </summary>
    public class EventBusInstaller : MonoInstaller
    {
        /// <summary>
        ///     Zenject method that is used to create all the bindings
        ///     in the dependency container.
        /// </summary>
        public override void InstallBindings()
        {
            Container.Bind<EventBus>().AsSingle();
        }
    }
}